import React, {  useState, useEffect } from 'react';
const StreamFile = require('stream-file');

function Player(props) {
    function stream () {
        const stream = new StreamFile({
        url: 'https://upload.wikimedia.org/wikipedia/commons/9/94/Folgers.ogv',
      
        // Optional; max size of each download chunk
        chunkSize: 1 * 1024 * 1024,
      
        // Optional; total amount of in-memory cache
        cacheSize: 32 * 1024 * 1024
      });
      return stream;
    }
    async function demo(stream) {
        try {
          // load() opens up an HTTP request and gets some state info.
          await stream.load();
          console.log(stream.seekable ? 'stream is seekable' : 'stream is not seekable');
          console.log('stream length',  stream.length);
          console.log('stream headers', stream.headers);
      
          // seek() moves the input point to a new position, if stream is seekable
          await stream.seek(1024);
      
          // read() waits until given byte count is available (or eof) and returns buffer
          let buffer = await stream.read(65536);
          console.log('read buffer with ' + buffer.byteLength + ' bytes');
          console.log(stream.eof ? 'at eof' : 'not at eof');
          console.log(stream.buffered); // buffered ranges
      
          // All done!
          stream.close();
      
        } catch (err) {
          // Any error conditions chain through the Promises to the final catch().
          console.log('failed', err)
        }
      }

      useEffect(() => {
        demo(stream())
      },[]);

    return(
        <p >Click on an emoji to view the emoji short name.</p>
      )
}
export default Player;
