import logo from './logo.svg';
import Player from './component/Player'
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Player />
      </header>
    </div>
  );
}

export default App;
